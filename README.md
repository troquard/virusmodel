# Synopsis #

GAMA models to simulate scenarios of virus propagations.

# Simulating the propagation of a virus in the city of Bozen-Bolzano #

We ran three simulations: when 10% of the population is respecting social distancing, when 60% does, and when 90% does. The results are in the directory REPORT.
