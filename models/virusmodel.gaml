/**
* Name: First Virus Model
* Author: nico
* Description: A first model of virus propagation in a population. 
* Parameters include, among others, the probability of high mobility and of social distancing.
*/

model virusmodel

global {
	// main parameters
	float step <- 1 #s;
	float time_simulation <- 500 #s; // simulated time until the simulation is paused
	
	int nb_people <- 1000;
	float side <- 400.0 #m;
	geometry shape <- square(side);
	
	float base_speed <- 10.0 #km/#h; // base speed of a person
	float variable_speed <- 5.0 #km/#h; // range of speed that will be added or substracted to base_speed
	float high_mobility_speed <- 100.0 #km/#h; // added speed to high mobility person
	
	// other parameters (can be changed through gui)
	float distance_infection <- 5 #m;
	
	float time_at_risk_after_contact <- step #s;
	float time_recovery <- 100 #s;
	
	float probability_initially_infected <- 0.1;
	float probability_infection <- 0.05;
	float probability_social_distancing <- 0.6;
	float probability_high_mobility <- 0.1;
	
	// colors
	rgb color_infected <- #red;
	rgb color_healthy <- #green;
	rgb color_recovered <- #orange;
	
	// monitored variables
	int num_infected <- 0 update: person count (each.state='infected');
	int num_healthy  <- 0 update: person count (each.state='healthy');
	int num_recovered  <- 0 update: person count (each.state='recovered');
	
	reflex pause {
		if (time > time_simulation) {
			do pause;	
		}
	}
	
	init{
		create person number:nb_people;
		// speed, high mobility, and social distancing
		// if social distancing, speed is 0
		// otherwise speed is some base speed, plus/minus some variable speed, and plus some high mobility speed with some probability
		ask person {
			if (flip(probability_social_distancing)) {
				speed <- 0.0 #km/#h;
			}
			else {
				bool high_mobility <- flip(probability_high_mobility);
				speed <- base_speed + rnd(-variable_speed, variable_speed) + (high_mobility?high_mobility_speed:0.0) #km/#h;
			}
		}
		// initial infections
		ask person {
			if (flip(probability_initially_infected)) {
				is_initially_infected <- true;
			}
		}
	}
}

species person skills:[moving] control: fsm {
	float speed;
	bool is_initially_infected;
	bool infected_contact <- false;
	float last_infected_contact_time <- 0.0; // in seconds
	float time_infected <- 0.0; // in seconds
	
	// fsm
	state init initial: true {
		enter {
		}
		transition to: infected when: self.is_initially_infected {
        	write string(cycle) + ":" + name + "->" + "initially infected";
    	}
    	transition to: healthy when: !self.is_initially_infected {
        	write string(cycle) + ":" + name + "->" + "initially healthy";
    	}
	}
	state healthy {
		enter {
		}
		transition to: infected when: self.infected_contact 
									and time - self.last_infected_contact_time <= time_at_risk_after_contact 
									and flip(probability_infection) {
        	write string(cycle) + ":" + name + "->" + "infected";
    	}
	}
	state infected {
		enter {
			self.time_infected <- time;
		}
		transition to: recovered when: time - self.time_infected >= time_recovery {
        	write string(cycle) + ":" + name + "->" + "recovered";
    	}
	}
	state recovered final: true {
		enter {
		}
	}
	
	
	// reflexes
	reflex move {
		do wander;
	}
	reflex infected_contact when: state='infected' {
		ask person at_distance distance_infection {
			self.infected_contact <- true;
			self.last_infected_contact_time <- time;
		}
	}
	
	
	// aspect
	rgb get_color {
		if (state = 'healthy') {
			return color_healthy;
		}
		else if (state = 'infected') {
			return color_infected;
		}
		else if (state = 'recovered') {
			return color_recovered;
		}
		else {
			return #grey;
		}
	}
	aspect circle {
		draw circle(side / 100) color:get_color();
	}
}

experiment main type: gui {
	parameter "Distance infection" var: distance_infection min: 1.0 max: 20.0;
	parameter "Time at risk after contact" var: time_at_risk_after_contact min: 1.0 max: 100.0;
	parameter "Time recovery" var: time_recovery min: 1.0 max: 1000.0;
	parameter "Probability infected at the start" var: probability_initially_infected min: 0.0 max: 1.0;
	parameter "Probability infection on contact" var: probability_infection min: 0.0 max: 1.0;
	parameter "Probability social distancing" var: probability_social_distancing min: 0.0 max: 1.0;
	parameter "Probability high mobility" var: probability_high_mobility min: 0.0 max: 1.0;
	
	output {
		monitor "number infected" value: num_infected;
		monitor "number healthy" value: num_healthy;
		monitor "number recovered" value: num_recovered;
		
		display chart_display refresh: every(5 #cycle) {
			chart "Population" type: series {
				data "infected" value: num_infected color: color_infected;
				data "healthy" value: num_healthy color: color_healthy;
				data "recovered" value: num_recovered color: color_recovered;
			}
		}
		
		display map {
			species person aspect:circle;	
		}
	}
}
