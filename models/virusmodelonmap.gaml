/**
* Name: Virus Model On Map
* Author: nico
* Description: A model of virus propagation in a population on a map. 
*/

model virusmodelonmap

global {
	// main parameters
	float step <- 3 #s;
	float time_simulation <- 1000 #s; // simulated time until the simulation is paused
	
	int nb_people <- 300;
	
	// Bolzano
	file roads_shapefile <- file("../includes/EL_STR_line.shp");
	file buildings_shapefile <- file("../includes/CadastralParcels_polygon.shp");
	
	geometry shape <- envelope(roads_shapefile);
	graph road_network;
	
	float base_speed <- 10.0 #km/#h; // base speed of a person
	float variable_speed <- 5.0 #km/#h; // range of speed that will be added or substracted to base_speed
	float high_mobility_speed <- 50.0 #km/#h; // added speed to high mobility person
	
	// other parameters (can be changed through gui)
	float distance_infection <- 10 #m;
	
	float time_at_risk_after_contact <- 3 * step #s;
	float time_recovery <- 300 #s;
	
	float probability_initially_infected <- 0.01;
	float probability_infection <- 0.25;
	float probability_social_distancing <- 0.1;
	float probability_high_mobility <- 0.2;
	
	// colors
	rgb color_infected <- #red;
	rgb color_healthy <- #green;
	rgb color_recovered <- #orange;
	
	// monitored variables
	int num_infected <- 0 update: person count (each.state='infected');
	int num_healthy  <- 0 update: person count (each.state='healthy');
	int num_recovered  <- 0 update: person count (each.state='recovered');
	
	
	reflex pause {
		if (time > time_simulation) {
			do pause;	
		}
	}
	
	init{
		create road from: roads_shapefile;
		road_network <- as_edge_graph(road);
		create building from: buildings_shapefile;
		create person number: nb_people {
			home <- one_of(building);
			home_point <- any_location_in(home);
			location <- home_point;
		}
		// speed, high mobility, and social distancing
		ask person {
			if (flip(probability_social_distancing)) {
				self.social_distancing <- true;
				speed <- base_speed + rnd(-variable_speed, variable_speed) #km/#h;
			}
			else {
				self.social_distancing <- false;
				self.high_mobility <- flip(probability_high_mobility);
				speed <- base_speed + rnd(-variable_speed, variable_speed) + (high_mobility?high_mobility_speed:0.0) #km/#h;
			}
		}
		// initial infections
		ask person {
			if (flip(probability_initially_infected)) {
				is_initially_infected <- true;
			}
		}
	}
}

species road {
	aspect geom {
		draw shape color: #black;
	}
}

species building {
	aspect geom {
		draw shape color: blend(#grey, #transparent);
	}
}

species person skills:[moving] control: fsm {
	bool social_distancing <- false;
	bool high_mobility <- false;
	float speed;
	bool is_initially_infected <- false;
	bool infected_contact <- false;
	float last_infected_contact_time <- 0.0; // in seconds
	float time_infected <- 0.0; // in seconds
	building home <- nil;
	point home_point <- nil;
	point target <- nil;
		
	// fsm
	state init initial: true {
		enter {
		}
		transition to: infected when: self.is_initially_infected {
        	write string(cycle) + ":" + name + "->" + "initially infected";
    	}
    	transition to: healthy when: !self.is_initially_infected {
        	write string(cycle) + ":" + name + "->" + "initially healthy";
    	}
	}
	state healthy {
		enter {
		}
		transition to: infected when: self.infected_contact 
									and time - self.last_infected_contact_time <= time_at_risk_after_contact 
									and flip(probability_infection) {
        	write string(cycle) + ":" + name + "->" + "infected";
    	}
	}
	state infected {
		enter {
			self.time_infected <- time;
		}
		transition to: recovered when: time - self.time_infected >= time_recovery {
        	write string(cycle) + ":" + name + "->" + "recovered";
    	}
	}
	state recovered final: true {
		enter {
		}
	}
	
	
	// reflexes
	reflex acquire_target when: target = nil {
		if (social_distancing) {
			self.target <- any_location_in(self.home);
		}
		else {
			point a_target <- any_location_in(one_of(building));
			self.target <- a_target;
		}
	}
	reflex move when: target != nil {
		if (social_distancing) {
			do goto target: self.target on: self.home;
		}
		else {
			do goto target: self.target on: road_network;
		}
		if (location = self.target) {
				self.target <- nil;
		}
	}
	reflex infected_contact when: state='infected' {
		ask person at_distance distance_infection {
			self.infected_contact <- true;
			self.last_infected_contact_time <- time;
		}
	}
	
	
	// aspect
	rgb get_color {
		if (state = 'healthy') {
			return color_healthy;
		}
		else if (state = 'infected') {
			return color_infected;
		}
		else if (state = 'recovered') {
			return color_recovered;
		}
		else {
			return #grey;
		}
	}
	aspect circle {
		draw circle(8) color:get_color();
	}
}

experiment main type: gui {
	parameter "Distance infection" var: distance_infection min: 1.0 max: 20.0;
	parameter "Time at risk after contact" var: time_at_risk_after_contact min: 1.0 max: 100.0;
	parameter "Time recovery" var: time_recovery min: 1.0 max: 1000.0;
	parameter "Probability infected at the start" var: probability_initially_infected min: 0.0 max: 1.0;
	parameter "Probability infection on contact" var: probability_infection min: 0.0 max: 1.0;
	parameter "Probability social distancing" var: probability_social_distancing min: 0.0 max: 1.0;
	parameter "Probability high mobility" var: probability_high_mobility min: 0.0 max: 1.0;
	
	output {
		monitor "number infected" value: num_infected;
		monitor "number healthy" value: num_healthy;
		monitor "number recovered" value: num_recovered;
		
		display chart_display refresh: every(2 #cycle) autosave: true {
			chart "Population" type: series {
				data "infected" value: num_infected color: color_infected;
				data "healthy" value: num_healthy color: color_healthy;
				data "recovered" value: num_recovered color: color_recovered;
			}
		}
		
		display map refresh: every(2 #cycle) autosave: true {
			image "../includes/bolzano.png" refresh:false; 
			species road aspect:geom;
			species building aspect:geom;
			species person aspect:circle;
		}
	}
}
